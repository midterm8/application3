package com.siriphonpha.application3

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.siriphonpha.application3.databinding.ActivityMainBinding
import com.siriphonpha.application3.model.Oil

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val data = listOf(
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20", 36.84),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91", 37.68),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95", 37.95),
            Oil("เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95", 45.44),
            Oil("เชลล์ ดีเซล B20", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล B7", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล B7", 47.06)
        )
        val recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ItemAdapter(data, applicationContext)
    }

    class ItemAdapter(private val data: List<Oil>, private val context: Context) :
        RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val title = itemView.findViewById<TextView>(R.id.item_title)!!
            val price = itemView.findViewById<TextView>(R.id.item_price)!!
            val item = itemView.findViewById<LinearLayout>(R.id.item)!!
        }
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.item_list,parent,false)
            return ViewHolder(adapterLayout)
        }
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.title.text = item.title
            holder.price.text = item.price.toString()
            holder.item.setOnClickListener {
                Toast.makeText(
                    context,
                    "${item.price} ${item.title}"
                    ,Toast.LENGTH_LONG)
                    .show()
            }
        }

        override fun getItemCount(): Int {
            return data.size
        }
    }
}